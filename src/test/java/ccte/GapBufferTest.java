package ccte;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class GapBufferTest {
	private GapBuffer gb;
	private final String TEST_DATA = "12345 67890\nqwerty uiop";

	@Before
	public void init() {
		gb = new GapBuffer();
		gb.insert(TEST_DATA);
		gb.moveHome();
	}

	@Test
	public void testMove() {
		assertEquals(gb.getStartIndex(), 0);
		gb.moveForward(2);
		assertEquals(gb.getStartIndex(), 2);
		gb.moveBackward();
		gb.moveBackward(1);
		assertEquals(gb.getStartIndex(), 0);
		gb.moveBackward();
		assertEquals(gb.getStartIndex(), 0);
		gb.moveForward(100);
		assertEquals(gb.getStartIndex(), 23);
	}

	@Test
	public void testInsert() {
		gb.insert('f');
		assertEquals(gb.toString(), "f" + TEST_DATA);
		gb.moveEnd();
		gb.insert("sss");
		assertEquals(gb.toString(), "f" + TEST_DATA + "sss");
		gb.moveBackward(3);
		gb.insert("ddd ");
		assertEquals(gb.toString(), "f" + TEST_DATA + "ddd sss");
	}

	@Test
	public void testMoveByWord() {
		assertEquals(gb.getStartIndex(), 0);
		gb.moveWordForward();
		assertEquals(gb.getStartIndex(), 6);
		gb.moveWordForward();
		gb.moveWordForward();
		gb.moveWordForward();
		assertEquals(gb.getStartIndex(), 23);
		gb.moveWordBackward();
		assertEquals(gb.getStartIndex(), 12);
		gb.moveWordBackward();
		assertEquals(gb.getStartIndex(), 6);
		gb.moveWordBackward();
		gb.moveWordBackward();
		assertEquals(gb.getStartIndex(), 0);
	}

	@Test
	public void testMoveLine() {
		assertEquals(gb.getStartIndex(), 0);
		gb.moveToLineEnd();
		assertEquals(gb.getStartIndex(), 11);
		gb.moveToLineBeginning();
		assertEquals(gb.getStartIndex(), 0);
		
		gb.moveToLineEnd();
		gb.moveForward();
		assertEquals(gb.getStartIndex(), 12);
		gb.moveToLineEnd();
		assertEquals(gb.getStartIndex(), 23);
		gb.moveToLineBeginning();
		assertEquals(gb.getStartIndex(), 12);
	}
	
	@Test
	public void testMoveUpDown() {
		gb.moveForward(3);
		assertEquals(gb.getStartIndex(), 3);
		gb.moveDown();
		assertEquals(gb.getStartIndex(), 15);
		
		gb.moveForward(2);
		assertEquals(gb.getStartIndex(), 17);
		gb.moveUp();
		assertEquals(gb.getStartIndex(), 5);
		gb.moveUp();
		assertEquals(gb.getStartIndex(), 0);
		
		gb.moveDown();
		gb.moveDown();
		assertEquals(gb.getStartIndex(), 23);
	}
}
