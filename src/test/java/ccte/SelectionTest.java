package ccte;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class SelectionTest {
	private GapBuffer gb;
	private final String TEST_DATA = "12345 67890\nqwerty uiop\n\nas df";
	private Selection selection;
	
	@Before
	public void init() {
		gb = new GapBuffer();
		gb.insert(TEST_DATA);
		gb.moveHome();
	}
	
	@Test
	public void test() {
		selection = new Selection(gb);
		String str = selection.getSelectedString();
		assertEquals(str, "");
		selection.moveForward();
		str = selection.getSelectedString();
		assertEquals(str, "1");
		selection.moveDown();
		str = selection.getSelectedString();
		assertEquals(str, "12345 67890\nq");
		selection.moveForward(3);
		str = selection.getSelectedString();
		assertEquals(str, "12345 67890\nqwer");
		selection.moveWordForward();
		str = selection.getSelectedString();
		assertEquals(str, "12345 67890\nqwerty ");
		selection.moveWordForward();
		str = selection.getSelectedString();
		assertEquals(str, "12345 67890\nqwerty uiop\n\n");
		selection.moveWordBackward();
		str = selection.getSelectedString();
		assertEquals(str, "12345 67890\nqwerty ");
		selection.moveUp();
		str = selection.getSelectedString();
		assertEquals(str, "12345 6");
	}
	
	@Test
	public void test2() {
		gb.moveForward(5);
		assertEquals(gb.getStartIndex(), 5);
		
		selection = new Selection(gb);
		String str = selection.getSelectedString();
		assertEquals(str, "");
		selection.moveBackward(4);
		str = selection.getSelectedString();
		assertEquals(str, "2345");
		selection.moveForward(7);
		str = selection.getSelectedString();
		assertEquals(str, " 67");
		selection.moveToLineEnd();
		str = selection.getSelectedString();
		assertEquals(str, " 67890");
		selection.moveToLineBeginning();
		str = selection.getSelectedString();
		assertEquals(str, "12345");
	}

}
