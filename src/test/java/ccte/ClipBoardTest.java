package ccte;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class ClipBoardTest {
	private ClipBoard clipBoard;
	
	@Before
	public void init() {
		clipBoard = new ClipBoard(3);
	}
	
	@Test
	public void test() {
		try {
			String str = clipBoard.getString(1);
			assertEquals(str, "");
			
			clipBoard.putString("aaa");
			clipBoard.putString("bbb");
			str = clipBoard.getString(2);
			assertEquals(str, "aaa");
			
			clipBoard.putString("ccc");
			clipBoard.putString("ddd");
			clipBoard.putString("eee");
			str = clipBoard.getString(2);
			assertEquals(str, "ddd");
			
			str = clipBoard.getString(3);
			assertEquals(str, "ccc");
		} catch (CCTEException e) {
			e.printStackTrace();
		}
	}
	
	@Test(expected = CCTEException.class)
	public void testWrongIndex() throws CCTEException {
		clipBoard.getString(0);
	}

}
