package ccte;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

public class AppTest {
	private App app;

	@Before
	public void init() {
		try {
			File file = new File("test1.txt");
			if (!file.exists()) {
				new FileOutputStream(file).close();
			}
			BufferedWriter bw = null;
			try {
				bw = new BufferedWriter(new FileWriter(file));
				bw.write("1234");
			} finally {
				if (bw != null) {
					bw.close();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String readFile(String fileName) throws CCTEException {
		try {
			FileInputStream fis = null;
			try {
				StringBuilder strb = new StringBuilder();
				fis = new FileInputStream(fileName);
				char ch;
				while (fis.available() > 0) {
					ch = (char) fis.read();
					strb.append(ch);
				}
				return strb.toString();
			} finally {
				if (fis != null) {
					fis.close();
				}
			}
		} catch (FileNotFoundException e) {
			throw new CCTEException("file not found");
		} catch (IOException e) {
			throw new CCTEException("file cannot be read");
		}
	}

	@Test
	public void test() {
		String[] args = new String[] { "test1.txt", "i", "\nqwert yuiop ", "0", "h", "i", "56789", "k", "l", "2", "i",
				"\n\nsdf g ", "o", "test2.txt" };
		try {
			app = new App(args);
			app.execute();
			String str = "56789\nqwert y\n\nsdf g uiop 1234";
			String fileStr = readFile(args[args.length - 1]);
			assertEquals(str, fileStr);
		} catch (CCTEException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test2() {
		String[] args = new String[] { "test1.txt", "l", "4", "i", " 5678", "v", "h", "4", "y", "0", "p", "i", " ", "o",
				"test3.txt" };
		try {
			app = new App(args);
			app.execute();
			String str = "5678 1234 5678";
			String fileStr = readFile(args[args.length - 1]);
			assertEquals(str, fileStr);
		} catch (CCTEException e) {
			e.printStackTrace();
		}
	}

}
