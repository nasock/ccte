package ccte;

public class Selection implements Mover {
	private GapBuffer gBuffer;
	private int beginIndex;
	private int stopIndex;

	public Selection(GapBuffer gb) {
		gBuffer = gb;
		beginIndex = gb.getStartIndex();
		stopIndex = beginIndex;
	}

	public void moveForward() {
		moveForward(1);
	}

	public void moveForward(int count) {
		stopIndex = gBuffer.selectForward(stopIndex, count);
	}

	public void moveWordForward() {
		stopIndex = gBuffer.selectWordForward(stopIndex);
	}

	public void moveToLineEnd() {
		stopIndex = gBuffer.selectToLineEnd(stopIndex);
	}

	public void moveDown() {
		stopIndex = gBuffer.selectDown(stopIndex);
	}

	public void moveBackward() {
		moveBackward(1);
	}

	public void moveBackward(int count) {
		stopIndex = gBuffer.selectBackward(stopIndex, count);
	}

	public void moveWordBackward() {
		stopIndex = gBuffer.selectWordBackward(stopIndex);
	}

	public void moveToLineBeginning() {
		stopIndex = gBuffer.selectToLineBeginning(stopIndex);
	}

	public void moveUp() {
		stopIndex = gBuffer.selectUp(stopIndex);
	}

	public void moveHome() {
		stopIndex = gBuffer.selectHome();
	}

	public void moveEnd() {
		stopIndex = gBuffer.selectEnd();
	}

	public int getBeginningIndex() {
		if (stopIndex < beginIndex) {
			return stopIndex;
		}
		int normalizedBeginingIndex = gBuffer.normalizeBeginningIndex();
		return normalizedBeginingIndex;
	}

	public int getStopIndex() {
		if (stopIndex < beginIndex) {
			return beginIndex;
		}
		return stopIndex;
	}
	
	public String getSelectedString() {
		return gBuffer.getSubstring(beginIndex, stopIndex);
	}

}
