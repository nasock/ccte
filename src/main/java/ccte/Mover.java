package ccte;

public interface Mover {
	
	public void moveForward();

	public void moveForward(int count);

	public void moveWordForward();

	public void moveToLineEnd();

	public void moveDown();

	public void moveBackward();

	public void moveBackward(int count);

	public void moveWordBackward();

	public void moveToLineBeginning();

	public void moveUp();

	public void moveHome();

	public void moveEnd();
}
