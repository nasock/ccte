package ccte;

import java.util.ArrayList;

public class LineParser {

	private void makeString(String line, int startIndex, int endIndex, ArrayList<String> strings) {
		String str = line.substring(startIndex, endIndex);
		str = str.trim();
		if (str != null && !str.equals("")) {
			strings.add(str);
		}
	}

	public ArrayList<String> parse(String line) {
		line = line.trim();
		ArrayList<String> strings = new ArrayList<String>();
		char[] chars = line.toCharArray();
		int startIndex = 0;
		int endIndex = 0;
		int countQuates = 0;
		for (int i = 0; i < chars.length; i++) {
			char ch = chars[i];
			
			if (ch == ' ' && countQuates == 0) {
				endIndex = i;
				makeString(line, startIndex, endIndex, strings);
				startIndex = endIndex + 1;
			} else if (ch == '"') {
				if (countQuates == 0) {
					countQuates = 1;
					startIndex = i + 1;
				} else if (countQuates == 1) {
					countQuates = 0;
					endIndex = i;
					makeString(line, startIndex, endIndex, strings);
					startIndex = endIndex + 1;
				}
			} else if (i == chars.length - 1) {
				endIndex = chars.length;
				makeString(line, startIndex, endIndex, strings);
			}
		}

		return strings;
	}

}
