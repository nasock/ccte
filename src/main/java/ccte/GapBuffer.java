package ccte;

public class GapBuffer implements Mover {
	private char[] chars;
	private int startIndex;
	private int endIndex;

	public GapBuffer() {
		makeNew();
	}

	private void makeNew() {
		chars = new char[2];
		startIndex = 0;
		endIndex = chars.length - 1;
	}

	private void resize() {
		char[] newChars = new char[chars.length * 2];
		System.arraycopy(chars, 0, newChars, 0, startIndex);
		System.arraycopy(chars, endIndex + 1, newChars, startIndex + 1 + chars.length, chars.length - 1 - endIndex);
		endIndex = startIndex + chars.length;
		chars = newChars;
	}

	public int getStartIndex() {
		return startIndex;
	}

	private int getDistanceToLineBeginning(int beginningIndex) {
		int countToLineBeginning = 0;
		int index = beginningIndex;
		if (index > 0) {
			char previousChar = chars[index - 1];
			while (!(previousChar == '\n' || previousChar == '\r')) {
				countToLineBeginning = countToLineBeginning + 1;
				index = index - 1;
				if (index == 0) {
					return countToLineBeginning;
				}
				if (index >= startIndex && index <= endIndex) {
					return countToLineBeginning - 1;
				}
				previousChar = chars[index - 1];
			}
		}
		return countToLineBeginning;
	}

	public void moveForward() {
		if (endIndex == chars.length - 1) {
			return;
		}
		chars[startIndex] = chars[endIndex + 1];
		startIndex = startIndex + 1;
		endIndex = endIndex + 1;
	}

	public void moveForward(int count) {
		for (int i = 0; i < count; i++) {
			moveForward();
		}
	}

	public void moveWordForward() {
		if (endIndex == chars.length - 1) {
			return;
		}

		char nextChar = chars[endIndex + 1];
		while (!((nextChar == '\n' || nextChar == '\r') || Character.isWhitespace(nextChar))) {
			moveForward();
			if (endIndex == chars.length - 1) {
				return;
			}
			nextChar = chars[endIndex + 1];
		}
		while ((nextChar == '\n' || nextChar == '\r') || Character.isWhitespace(nextChar)) {
			moveForward();
			if (endIndex == chars.length - 1) {
				return;
			}
			nextChar = chars[endIndex + 1];
		}
	}

	public void moveToLineEnd() {
		if (endIndex == chars.length - 1) {
			return;
		}

		char nextChar = chars[endIndex + 1];
		while (!(nextChar == '\n' || nextChar == '\r')) {
			moveForward();
			if (endIndex == chars.length - 1) {
				return;
			}
			nextChar = chars[endIndex + 1];
		}
	}

	public void moveDown() {
		if (endIndex == chars.length - 1) {
			return;
		}

		int countToLineBeginning = getDistanceToLineBeginning(startIndex);
		moveToLineEnd();
		moveForward();
		for (int i = 0; i < countToLineBeginning; i++) {
			if (endIndex == chars.length - 1) {
				return;
			}
			char nextChar = chars[endIndex + 1];
			if (nextChar == '\n' || nextChar == '\r') {
				break;
			}
			moveForward();
		}
	}

	public void moveBackward() {
		if (startIndex == 0) {
			return;
		}
		chars[endIndex] = chars[startIndex - 1];
		startIndex = startIndex - 1;
		endIndex = endIndex - 1;
	}

	public void moveBackward(int count) {
		for (int i = 0; i < count; i++) {
			moveBackward();
		}
	}

	public void moveWordBackward() {
		if (startIndex == 0) {
			return;
		}

		char previousChar = chars[startIndex - 1];
		while (!((previousChar == '\n' || previousChar == '\r') || Character.isWhitespace(previousChar))) {
			moveBackward();
			if (startIndex == 0) {
				return;
			}
			previousChar = chars[startIndex - 1];
		}
		while ((previousChar == '\n' || previousChar == '\r') || Character.isWhitespace(previousChar)) {
			moveBackward();
			if (startIndex == 0) {
				return;
			}
			previousChar = chars[startIndex - 1];
		}
		while (!((previousChar == '\n' || previousChar == '\r') || Character.isWhitespace(previousChar))) {
			moveBackward();
			if (startIndex == 0) {
				return;
			}
			previousChar = chars[startIndex - 1];
		}
	}

	public void moveToLineBeginning() {
		if (startIndex == 0) {
			return;
		}

		char previousChar = chars[startIndex - 1];
		while (!(previousChar == '\n' || previousChar == '\r')) {
			moveBackward();
			if (startIndex == 0) {
				return;
			}
			previousChar = chars[startIndex - 1];
		}
	}

	public void moveUp() {
		if (startIndex == 0) {
			return;
		}
		int countToLineBeginning = getDistanceToLineBeginning(startIndex);
		moveToLineBeginning();
		if (startIndex == 0) {
			return;
		}
		moveBackward();
		moveToLineBeginning();
		for (int i = 0; i < countToLineBeginning; i++) {
			if (endIndex == chars.length - 1) {
				return;
			}
			char nextChar = chars[endIndex + 1];
			if (nextChar == '\n' || nextChar == '\r') {
				break;
			}
			moveForward();
		}
	}

	public void moveHome() {
		while (startIndex != 0) {
			moveBackward();
		}
	}

	public void moveEnd() {
		while (endIndex != chars.length - 1) {
			moveForward();
		}
	}

	public int normalizeBeginningIndex() {
		return endIndex + 1;
	}

	public int selectForward(int stopInd, int count) {
		if (stopInd >= chars.length) {
			return chars.length;
		}
		int newStopInd = stopInd;
		while (newStopInd < startIndex && count > 0) {
			newStopInd = newStopInd + 1;
			count = count - 1;
		}
		if (count > 0) {
			if (newStopInd == startIndex) {
				newStopInd = endIndex + 1;
			}
			while (count > 0) {
				newStopInd = newStopInd + 1;
				count = count - 1;
			}
		}
		if (newStopInd >= chars.length) {
			newStopInd = chars.length;
		}
		return newStopInd;
	}

	public int selectWordForward(int stopInd) {
		if (stopInd >= chars.length) {
			return chars.length;
		}
		int newStopInd = stopInd;
		char nextChar = chars[newStopInd];
		while (!((nextChar == '\n' || nextChar == '\r') || Character.isWhitespace(nextChar))) {
			newStopInd = selectForward(newStopInd, 1);
			if (stopInd >= chars.length) {
				return chars.length;
			}
			nextChar = chars[newStopInd];
		}
		while ((nextChar == '\n' || nextChar == '\r') || Character.isWhitespace(nextChar)) {
			newStopInd = selectForward(newStopInd, 1);
			if (stopInd >= chars.length) {
				return chars.length;
			}
			nextChar = chars[newStopInd];
		}
		return newStopInd;
	}

	public int selectToLineEnd(int stopInd) {
		if (stopInd >= chars.length) {
			return chars.length;
		}
		int newStopInd = stopInd;
		char nextChar = chars[newStopInd];
		while (!(nextChar == '\n' || nextChar == '\r')) {
			newStopInd = selectForward(newStopInd, 1);
			if (newStopInd >= chars.length) {
				return chars.length;
			}
			nextChar = chars[newStopInd];
		}
		return newStopInd;
	}

	public int selectDown(int stopInd) {
		if (stopInd >= chars.length) {
			return chars.length;
		}
		int countToLineBeginning = getDistanceToLineBeginning(stopInd);
		int newStopInd = stopInd;
		newStopInd = selectToLineEnd(newStopInd);
		if (newStopInd >= chars.length) {
			return chars.length;
		}
		newStopInd = selectForward(newStopInd, 1);
		for (int i = 0; i < countToLineBeginning; i++) {
			if (stopInd >= chars.length) {
				return chars.length;
			}
			char nextChar = chars[stopInd];
			if (nextChar == '\n' || nextChar == '\r') {
				break;
			}
			newStopInd = selectForward(newStopInd, 1);
		}
		return newStopInd;
	}

	public int selectBackward(int stopInd, int count) {
		if (stopInd <= 0) {
			return 0;
		}
		int newStopInd = stopInd;
		while (newStopInd > endIndex && count > 0) {
			newStopInd = newStopInd - 1;
			count = count - 1;
		}
		if (count > 0) {
			if (newStopInd == endIndex) {
				newStopInd = startIndex - 1;
			}
			while (count > 0) {
				newStopInd = newStopInd - 1;
				count = count - 1;
			}
		}
		if (stopInd <= 0) {
			newStopInd = 0;
		}
		return newStopInd;
	}

	public int selectWordBackward(int stopInd) {
		if (stopInd <= 0) {
			return 0;
		}
		int newStopInd = stopInd;
		char previousChar = chars[newStopInd - 1];
		while (!((previousChar == '\n' || previousChar == '\r') || Character.isWhitespace(previousChar))) {
			newStopInd = selectBackward(newStopInd, 1);
			if (newStopInd <= 0) {
				return 0;
			}
			previousChar = chars[newStopInd - 1];
		}
		while ((previousChar == '\n' || previousChar == '\r') || Character.isWhitespace(previousChar)) {
			newStopInd = selectBackward(newStopInd, 1);
			if (newStopInd <= 0) {
				return 0;
			}
			previousChar = chars[newStopInd - 1];
		}
		while (!((previousChar == '\n' || previousChar == '\r') || Character.isWhitespace(previousChar))) {
			newStopInd = selectBackward(newStopInd, 1);
			if (newStopInd <= 0) {
				return 0;
			}
			previousChar = chars[newStopInd - 1];
		}
		return newStopInd;
	}

	public int selectToLineBeginning(int stopInd) {
		if (stopInd <= 0) {
			return 0;
		}
		int newStopInd = stopInd;
		char previousChar = chars[newStopInd - 1];
		while (!(previousChar == '\n' || previousChar == '\r')) {
			newStopInd = selectBackward(newStopInd, 1);
			if (newStopInd <= 0) {
				return 0;
			}
			previousChar = chars[newStopInd - 1];
		}
		return newStopInd;
	}

	public int selectUp(int stopInd) {
		if (stopInd <= 0) {
			return 0;
		}
		int countToLineBeginning = getDistanceToLineBeginning(stopInd);
		int newStopInd = stopInd;
		newStopInd = selectToLineBeginning(newStopInd);
		if (stopInd <= 0) {
			return 0;
		}
		newStopInd = selectBackward(newStopInd, 1);
		newStopInd = selectToLineBeginning(newStopInd);

		for (int i = 0; i < countToLineBeginning; i++) {
			if (stopInd >= chars.length) {
				return chars.length;
			}
			char nextsChar = chars[newStopInd];
			if (nextsChar == '\n' || nextsChar == '\r') {
				break;
			}
			newStopInd = selectForward(newStopInd, 1);
		}
		return newStopInd;
	}

	public int selectHome() {
		return 0;
	}

	public int selectEnd() {
		return chars.length;
	}

	public void delRight() {
		if (endIndex == chars.length - 1) {
			return;
		}
		endIndex = endIndex + 1;
	}

	public void delLeft() {
		if (startIndex == 0) {
			return;
		}
		startIndex = startIndex - 1;
	}

	public void insert(char ch) {
		if (startIndex == endIndex) {
			resize();
		}
		chars[startIndex] = ch;
		startIndex = startIndex + 1;
	}

	public void insert(String line) {
		char[] chars = line.toCharArray();
		for (char ch : chars) {
			insert(ch);
		}
	}

	public void replace(char ch) {
		if (endIndex == chars.length - 1) {
			return;
		}
		chars[endIndex + 1] = ch;
	}

	public void clear() {
		makeNew();
	}

	public char[] toCharArray() {
		char[] charsToPrint = new char[startIndex + chars.length - 1 - endIndex];
		System.arraycopy(chars, 0, charsToPrint, 0, startIndex);
		System.arraycopy(chars, endIndex + 1, charsToPrint, startIndex, chars.length - 1 - endIndex);
		return charsToPrint;
	}

	public String getSubstring(int beginIndex, int stopIndex) {
		if (beginIndex == stopIndex) {
			return "";
		} else if (stopIndex < beginIndex) {
			int ind = stopIndex;
			stopIndex = beginIndex;
			beginIndex = ind;
		} else {
			beginIndex = normalizeBeginningIndex();
		}
		int length = stopIndex - beginIndex;
		char[] subChars = new char[length];
		System.arraycopy(chars, beginIndex, subChars, 0, length);
		return new String(subChars);
	}

	@Override
	public String toString() {
		return new String(toCharArray());
	}

}
