package ccte;

public class CCTEException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CCTEException(String string) {
		super(string);
	}
}
