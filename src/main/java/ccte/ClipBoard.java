package ccte;

public class ClipBoard {
	private int indexNext;
	private String[] strings;

	public ClipBoard(int size) {
		strings = new String[size + 1];
		for(int i = 0; i < strings.length; i++) {
			strings[i] = "";
		}
		indexNext = 0;
	}

	private int calculateGetIndex(int index) {
		if (index <= indexNext) {
			return indexNext - index;
		}
		int diff = index - indexNext;
		return strings.length - diff;
	}

	public void putString(String str) {
		strings[indexNext] = str;
		indexNext = indexNext + 1;
		if (indexNext > strings.length - 1) {
			indexNext = indexNext - strings.length;
		}
	}

	public String getString(int index) throws CCTEException {
		if (index > strings.length - 1 || index < 1) {
			throw new CCTEException("index out of bounds");
		}
		int getIndex = calculateGetIndex(index);
		return strings[getIndex];
	}

}
