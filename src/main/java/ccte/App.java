package ccte;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

public class App {
	private GapBuffer gapBuffer;
	public Selection selection;
	private File file;
	private ClipBoard clipBoard;
	private String[] arguments;

	public App(String[] args) throws CCTEException {
		gapBuffer = new GapBuffer();
		clipBoard = new ClipBoard(3);
		arguments = args;
		String pathStr = arguments[0];
		file = new File(pathStr);
		readFile(file);
	}

	public GapBuffer getGapBuffer() {
		return gapBuffer;
	}

	public void goHome() {
		gapBuffer.moveHome();
	}

	public void goEnd() {
		gapBuffer.moveEnd();
	}

	public void readFile(File file) throws CCTEException {
		try {
			if (!file.exists()) {
				new FileOutputStream(file).close();
			}
			FileInputStream fis = null;
			try {
				fis = new FileInputStream(file);
				char ch;
				while (fis.available() > 0) {
					ch = (char) fis.read();
					gapBuffer.insert(ch);
				}
				goHome();
			} finally {
				if (fis != null) {
					fis.close();
				}
			}
		} catch (FileNotFoundException e) {
			throw new CCTEException("file not found");
		} catch (IOException e) {
			throw new CCTEException("file cannot be read");
		}
	}

	public void insert(String line) {
		gapBuffer.insert(line);
	}

	public void replace(char ch) {
		gapBuffer.replace(ch);
	}

	public void clear() {
		gapBuffer.clear();
	}

	public void printout() {
		String str = new String(gapBuffer.toCharArray());
		System.out.println(str);
	}

	public void writeToFile(File fileToWrite) throws CCTEException {
		String str = new String(gapBuffer.toCharArray());
		try {
			if (!fileToWrite.exists()) {
				new FileOutputStream(fileToWrite).close();
			}
			BufferedWriter bw = null;
			try {
				bw = new BufferedWriter(new FileWriter(fileToWrite));
				bw.write(str);
			} finally {
				if (bw != null) {
					bw.close();
				}
			}
		} catch (IOException e) {
			throw new CCTEException("file cannot be read");
		}
	}

	public void execute() throws CCTEException {
		int i = 1;
		Mover mover = gapBuffer;
		while (i < arguments.length) {
			String command = arguments[i++];
			if (command.equals("v")) {
				selection = new Selection(gapBuffer);
				mover = selection;
			} else if (command.equals("pr")) {
				printout();
			} else if (command.equals("w")) {
				writeToFile(file);
			} else if (command.equals("o")) {
				if (i >= arguments.length) {
					throw new CCTEException("no file name");
				}
				String fileName = arguments[i++];
				File newFile = new File(fileName);
				writeToFile(newFile);
			} else if (command.equals("y")) {
				if (!(mover instanceof Selection)) {
					throw new CCTEException("wrong regime");
				}
				String str = selection.getSelectedString();
				clipBoard.putString(str);
				mover = gapBuffer;
			} else if (command.equals("i")) {
				if (i >= arguments.length) {
					throw new CCTEException("no string to insert");
				}
				String str = arguments[i++];
				insert(str);
			} else if (command.equals("r")) {
				if (i >= arguments.length) {
					throw new CCTEException("no string to insert");
				}
				String str = arguments[i++];
				str = str.trim();
				char[] chars = str.toCharArray();
				if (chars.length != 1) {
					throw new CCTEException("wrong char to replace");
				}
				replace(chars[0]);
			} else if (command.equals("p")) {
				int count = 1;
				if (i < arguments.length) {
					String integer = arguments[i];
					if (integer.matches("[+-]?[0-9]+")) {
						count = Integer.parseInt(integer);
						i++;
					}
				}
				String str = clipBoard.getString(count);
				insert(str);
			} else if (command.equals("l")) {
				int count = 1;
				if (i < arguments.length) {
					String integer = arguments[i];
					if (integer.matches("[+-]?[0-9]+")) {
						count = Integer.parseInt(integer);
						i++;
					}
				}
				if (count > 0) {
					mover.moveForward(count);
				} else {
					mover.moveBackward(count * -1);
				}
			} else if (command.equals("h")) {
				int count = 1;
				if (i < arguments.length) {
					String integer = arguments[i];
					if (integer.matches("[+-]?[0-9]+")) {
						count = Integer.parseInt(integer);
						i++;
					}
				}
				if (count > 0) {
					mover.moveBackward(count);
				} else {
					mover.moveForward(count * -1);
				}
			} else if (command.equals("j")) {
				mover.moveUp();
			} else if (command.equals("k")) {
				mover.moveDown();
			} else if (command.equals("w")) {
				mover.moveWordForward();
			} else if (command.equals("W")) {
				mover.moveWordBackward();
			} else if (command.equals("0")) {
				mover.moveToLineBeginning();
			} else if (command.equals("$")) {
				mover.moveToLineEnd();
			} else if (command.equals("gg")) {
				mover.moveHome();
			} else if (command.equals("G")) {
				mover.moveEnd();
			} else {
				throw new CCTEException("unknown command");
			}
		}
	}

	public static void main(String[] args) {
		if (args.length < 2) {
			System.out.println("insert path and command");
			return;
		}
		try {
			App app = new App(args);
			app.execute();
		} catch (CCTEException e) {
			e.printStackTrace();
		}
	}
}
